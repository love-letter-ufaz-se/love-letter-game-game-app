package main

import (
	"context"

	"gitlab.com/love-letter-ufaz-se/love-letter-game-game-app/v2/game_proto"
)

func (*server) NewGame(ctx context.Context, request *game_proto.Player) (*game_proto.Game, error) {
	gm, err := (player{
		id: request.Id,
	}).playerNewGame()

	if err != nil {
		return nil, err
	}
	return &game_proto.Game{
		Id: gm.id,
	}, nil
}

func (*server) StartGame(ctx context.Context, request *game_proto.Game) (*game_proto.NoMsg, error) {
	gm := game{
		id: request.Id,
	}
	gm.shuffleCards()
	gm.shareCardsFirst()
	return &game_proto.NoMsg{}, nil
}

func (*server) DrawCard(ctx context.Context, request *game_proto.GamePlayer) (*game_proto.Card, error) {
	gm := game{
		id: request.Gm.Id,
	}
	plyr := player{
		id: request.Plyr.Id,
	}
	response := &game_proto.Card{
		Id: int64(gm.drawCardFromDeck(plyr)),
	}
	return response, nil
}

func (*server) DiscardCard(ctx context.Context, request *game_proto.GamePlayerCard) (*game_proto.NoMsg, error) {
	gm := game{
		id: request.Gm.Id,
	}
	gm.discardCard(player{id: request.Plyr.Id}, request.Crd.Id)
	return &game_proto.NoMsg{}, nil
}

func (*server) WhoWins(ctx context.Context, request *game_proto.Game) (*game_proto.Player, error) {
	gm := game{
		id: request.Id,
	}
	winner := gm.whoWins()

	response := &game_proto.Player{
		Id: winner.id,
	}
	return response, nil
}
