package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net"
	"os"

	"github.com/go-redis/redis"
	"gitlab.com/love-letter-ufaz-se/love-letter-game-game-app/v2/game_proto"
	"gitlab.com/love-letter-ufaz-se/love-letter-game-game-app/v2/redismngr"
	"google.golang.org/grpc"
)

type player struct {
	id int64
}

type game struct {
	id int64
}

type card struct {
	Name       string `json:"name"`
	Num        int    `json:"num"`
	NumOfCards int    `json:"num_of_cards"`
	ID         int    `json:"id"`
}

type gameRules struct {
	Cards []card `json:"cards"`
}

var redisClient *redis.Client
var defaultGame gameRules

type server struct{}

func main() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})
	defer redisClient.Close()

	_, err := redisClient.Ping().Result()
	handleErr("Can't ping redis server", err)

	defaultCardsJSON, err := os.Open("default_cards.json")
	handleErr("Can not open defaultCardsJSON file", err)
	defer defaultCardsJSON.Close()
	defaultCardsByte, err := ioutil.ReadAll(defaultCardsJSON)
	handleErr("Can not read file defaultCardsJSON", err)
	err = json.Unmarshal(defaultCardsByte, &defaultGame)
	handleErr("Cannot unmarshal defaultCardsByte", err)

	address := "0.0.0.0:50051"
	lis, err := net.Listen("tcp", address)
	handleErr("Error while starting listen server for main", err)
	s := grpc.NewServer()
	game_proto.RegisterGameAppServer(s, &server{})
	s.Serve(lis)

}

func setExpirationOfSetMember(key string, val string, exp string) error {
	opts := grpc.WithInsecure()
	redismngrGRPC, err := grpc.Dial("redismngr-app:50051", opts)
	handleErr("Could not create connection between redismngr-app", err)
	defer redismngrGRPC.Close()

	// client := session.NewSessionServiceClient(sessionGRPC)
	client := redismngr.NewRedisMngrServiceClient(redismngrGRPC)

	request := &redismngr.RedisData{
		Key:          key,
		ElementValue: val,
		Expires:      exp,
	}

	_, err = client.NewSetMember(context.Background(), request)

	return err

}

func setExpirationofListMember(key string, val string, exp string) error {
	opts := grpc.WithInsecure()
	redismngrGRPC, err := grpc.Dial("redismngr-app:50051", opts)
	handleErr("Could not create connection between redismngr-app", err)
	defer redismngrGRPC.Close()

	// client := session.NewSessionServiceClient(sessionGRPC)
	client := redismngr.NewRedisMngrServiceClient(redismngrGRPC)

	request := &redismngr.RedisData{
		Key:          key,
		ElementValue: val,
		Expires:      exp,
	}

	_, err = client.NewListMember(context.Background(), request)

	return err
}

func handleErr(msg string, err error) {
	if err != nil {
		log.Fatalf("%s ... %v\n", msg, err)
	}
}
