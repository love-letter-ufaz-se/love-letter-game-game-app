package main

import (
	"math/rand"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

func (plyr player) playerNewGame() (game, error) {

	waitingGamesNum, err := redisClient.LLen("games:waiting").Result()
	if err != nil || waitingGamesNum == 0 {
		return plyr.createNewGame(), nil
	}
	waitingGamePeek, err := redisClient.LRange("games:waiting", 0, 0).Result()
	if err != nil {
		return plyr.createNewGame(), nil
	}
	waitingGamePlayerCount, err := redisClient.Incr("game:" + waitingGamePeek[0] + ":userCount").Result()

	if waitingGamePlayerCount > 4 {
		return plyr.playerNewGame()
	}
	if waitingGamePlayerCount == 4 {
		_, err = redisClient.LPop("games:waiting").Result()
		if err != nil {
			return plyr.playerNewGame()
		}
		_, err = redisClient.RPush("games:notify", waitingGamePeek[0]).Result()
	}

	_, err = redisClient.ZAdd("game:"+waitingGamePeek[0]+":users", redis.Z{Member: strconv.FormatInt(plyr.id, 10)}).Result()

	gameID, err := strconv.Atoi(waitingGamePeek[0])
	handleErr("Error when trying to convert waitingGamePeek to int", err)
	return game{
		id: int64(gameID),
	}, nil

}

func (plyr player) createNewGame() game {
	gameID := generateGameIDOnRedis()

	_, err := redisClient.Incr("game:" + strconv.Itoa(gameID) + ":userCount").Result()
	handleErr("Can't increment userCount of game in new game creation", err)

	_, err = redisClient.RPush("games:waiting", strconv.Itoa(gameID)).Result()
	setExpirationofListMember("games:waiting", strconv.Itoa(gameID), "2h")
	handleErr("Can't set game to waiting when creating a new game", err)

	_, err = redisClient.ZAdd("game:"+strconv.Itoa(gameID)+":users", redis.Z{Member: strconv.FormatInt(plyr.id, 10)}).Result()

	// Other game elements needed

	return game{
		id: int64(gameID),
	}
}

func (gm game) shuffleCards() {
	var cards []card
	for _, cardElem := range defaultGame.Cards {
		for i := 0; i < cardElem.NumOfCards; i++ {
			cards = append(cards, cardElem)
		}
	}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(cards), func(i, j int) { cards[i], cards[j] = cards[j], cards[i] })
	for _, cardElem := range cards {
		_, _ = redisClient.RPush("game:"+strconv.FormatInt(gm.id, 10)+":cards", cardElem.Num).Result()
	}
}

func (gm game) shareCardsFirst() {
	users, _ := redisClient.ZRange("game:"+strconv.FormatInt(gm.id, 10)+":users", 0, 5).Result()
	for _, user := range users {
		cardElem, _ := redisClient.LPop("game:" + strconv.FormatInt(gm.id, 10) + ":cards").Result()
		expDur, _ := time.ParseDuration("2h")
		redisClient.Set("game:"+strconv.FormatInt(gm.id, 10)+":user:"+user+":card", cardElem, expDur)
	}
}

func (gm game) drawCardFromDeck(plyr player) int {
	cardElem, _ := redisClient.LPop("game:" + strconv.FormatInt(gm.id, 10) + ":cards").Result()
	expDur, _ := time.ParseDuration("2h")
	redisClient.Set("game:"+strconv.FormatInt(gm.id, 10)+":user:"+strconv.FormatInt(plyr.id, 10)+":extra_card", cardElem, expDur)
	cardNum, _ := strconv.Atoi(cardElem)
	return cardNum
}

func (gm game) discardCard(plyr player, cardNum int64) {
	redisClient.SAdd("game:"+strconv.FormatInt(gm.id, 10)+":user:"+strconv.FormatInt(plyr.id, 10)+":disc_cards", strconv.FormatInt(cardNum, 10))
	currCard, _ := redisClient.Get("game:" + strconv.FormatInt(gm.id, 10) + ":user:" + strconv.FormatInt(plyr.id, 10) + ":card").Result()
	currCardInt, _ := strconv.Atoi(currCard)
	if cardNum == int64(currCardInt) {
		extraCard, _ := redisClient.Get("game:" + strconv.FormatInt(gm.id, 10) + ":user:" + strconv.FormatInt(plyr.id, 10) + ":extra_card").Result()
		expDur, _ := time.ParseDuration("2h")
		redisClient.Set("game:"+strconv.FormatInt(gm.id, 10)+":user:"+strconv.FormatInt(plyr.id, 10)+":card", extraCard, expDur)
	}
}

func (gm game) whoWins() player {
	users, _ := redisClient.ZRange("game:"+strconv.FormatInt(gm.id, 10)+":users", 0, 5).Result()
	bestUser := player{
		id: 0,
	}
	tieUser := player{
		id: 0,
	}
	highestCard := 0
	tieFlag := false
	for _, user := range users {
		userCard, _ := redisClient.Get("game:" + strconv.FormatInt(gm.id, 10) + ":user:" + user + ":card").Result()
		userCardNum, _ := strconv.Atoi(userCard)
		if userCardNum > highestCard {
			highestCard = userCardNum
			userInt, _ := strconv.Atoi(user)
			bestUser = player{
				id: int64(userInt),
			}
			tieFlag = false
		} else if userCardNum == highestCard {
			tieFlag = true
			userInt, _ := strconv.Atoi(user)
			tieUser = player{
				id: int64(userInt),
			}
		}
	}
	if tieFlag {
		tieCards, _ := redisClient.SMembers("game:" + strconv.FormatInt(gm.id, 10) + ":user:" + strconv.FormatInt(tieUser.id, 10) + ":disc_cards").Result()
		bestCards, _ := redisClient.SMembers("game:" + strconv.FormatInt(gm.id, 10) + ":user:" + strconv.FormatInt(bestUser.id, 10) + ":disc_cards").Result()

		tieCardsSum := 0
		bestCardsSum := 0

		for _, tieCard := range tieCards {
			cardNum, _ := strconv.Atoi(tieCard)
			tieCardsSum += cardNum
		}
		for _, bestCard := range bestCards {
			cardNum, _ := strconv.Atoi(bestCard)
			bestCardsSum += cardNum
		}
		if tieCardsSum > bestCardsSum {
			bestUser = tieUser
		}

	}

	return bestUser
}

func generateGameIDOnRedis() int {
	rand.Seed(time.Now().UnixNano())
	randNum := rand.Intn(100000)
	if ismem, err := redisClient.SIsMember("games", strconv.Itoa(randNum)).Result(); ismem || err != nil {
		handleErr("Error when trying to checnk if randNum is memeber of games in redis", err)
		return generateGameIDOnRedis()
	}
	_, err := redisClient.SAdd("games", strconv.Itoa(randNum)).Result()
	setExpirationOfSetMember("games", strconv.Itoa(randNum), "2h")
	if err != nil {
		return generateGameIDOnRedis()
	}
	return randNum
}
