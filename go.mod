module gitlab.com/love-letter-ufaz-se/love-letter-game-game-app/v2

go 1.14

require (
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/golang/protobuf v1.4.1
	google.golang.org/grpc v1.29.1
)
