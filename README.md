Game microservice that controls all in-game related concepts.

# All microservices

- session
- users
- parser (~API Gateway)
- Game
- ~Extension manager~
- Redis Set cleaner
